CLI_CONFIG = {}
CONFIG = {
    "use_cached_services": {
        "default": True,
        "help": "Use cached services when reloading from launchd",
    }
}
DYNE = {
    "exec": ["exec"],
    "states": ["states"],
    "corn": ["corn"],
}
