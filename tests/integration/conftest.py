import os
import pop.hub
import pytest
import sys
import unittest.mock as mock
import _pytest.config as conf
import _pytest.config.argparsing as argparse
from typing import List


@pytest.fixture(scope="session")
def hub():
    """
    provides a full hub that is used as a reference for mock_hub
    """
    hub = pop.hub.Hub()

    # strip pytest args
    with mock.patch.object(sys, "argv", sys.argv[:1]):
        hub.pop.sub.add(dyne_name="corn")
        hub.pop.sub.add(dyne_name="exec")
        hub.pop.sub.add(dyne_name="states")

    hub.corn.init.standalone()

    return hub


def pytest_addoption(parser: argparse.Parser):
    parser.addoption("--destructive", action="store_true")
    parser.addoption("--expensive", action="store_true")
    parser.addoption("--slow", action="store_true")


def pytest_configure(config: conf.Config):
    config.addinivalue_line("markers", "root(): Test requires root")
    config.addinivalue_line("markers", "destructive(): This test is destructive")
    config.addinivalue_line("markers", "expensive(): This test is expensive")
    config.addinivalue_line("markers", "slow(): This test is slow")


def pytest_collection_modifyitems(config: conf.Config, items: List[pytest.Function]):
    # Remove the destructive/expensive marker if their flag was set
    for item in items:
        for flag in ("destructive", "expensive", "slow"):
            mark = item.get_closest_marker(flag)
            if config.getoption(flag) and mark in item.own_markers:
                item.own_markers.remove(mark)


def pytest_runtest_setup(item: pytest.Function):
    if item.get_closest_marker("root") and os.getuid():
        pytest.skip("You must be logged in as root to run this test")

    if item.get_closest_marker("destructive"):
        pytest.skip("Destructive tests are disabled")

    if item.get_closest_marker("expensive"):
        pytest.skip("Expensive tests are disabled")

    if item.get_closest_marker("slow"):
        pytest.skip("Slow tests are disabled")
