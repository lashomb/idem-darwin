# import exec.services
import pytest
import shutil

SERVICE_NAME = "com.apple.apsd"


@pytest.fixture(scope="class")
async def setup_teardown(hub):
    # Set up and teardown the entire module, yielding a hub inbetween
    # Setup class
    # check if the service is enabled before starting the test
    # Put it back in it's original state when test is over
    #enabled = await hub.exec.service.enabled(SERVICE_NAME)
    yield hub
    # Tear down class


@pytest.fixture(scope="function")
async def s_hub(hub):
    # Setup and tear down each test, yielding a hub
    # Setup
    #enabled = await hub.exec.service.enabled(SERVICE_NAME)
    yield hub
    # Teardown


@pytest.mark.skipif(not shutil.which("launchctl"), reason="These tests require the `launchctl` binary")
@pytest.mark.skipif(not shutil.which("plutil"), reason="These tests require the `plutil` binary")
@pytest.mark.root
class TestService:
    """
    Validate the darwin service module
    """
    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_available(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_available_services(self, hub):
        services = await hub.exec.service.available_services()

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_confirm_updated(
            self, s_hub,
    ):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_disable(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_disabled(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_enable(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_enabled(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_get_all(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_get_enabled(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_list_(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_launchctl(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_missing(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_read_plist_file(self, hub):
        plist = await hub.exec.service.read_plist_file("/Library/LaunchDaemons", SERVICE_NAME)
        print("1" * 100)
        print("1" * 100)
        print(plist)
        print("1" * 100)
        print("1" * 100)

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_restart(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.slow
    async def test_show(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_start(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_status(self, s_hub):
        pass

    @pytest.mark.asyncio
    @pytest.mark.destructive
    @pytest.mark.slow
    async def test_stop(self, s_hub):
        pass

    @pytest.mark.slow
    def test_validate_enabled(self, s_hub):
        pass
