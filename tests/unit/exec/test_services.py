# import exec.services
import pytest


class TestServices:
    @pytest.mark.asyncio
    async def test_available(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_available_services(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_confirm_updated(
        self, c_hub,
    ):
        pass

    @pytest.mark.asyncio
    async def test_disable(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_disabled(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_enable(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_enabled(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_get_all(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_get_enabled(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_list_(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_launchctl(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_missing(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_read_plist_file(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_restart(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_show(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_start(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_status(self, c_hub):
        pass

    @pytest.mark.asyncio
    async def test_stop(self, c_hub):
        pass

    def test_validate_enabled(self, c_hub):
        pass
