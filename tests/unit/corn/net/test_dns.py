import idem_darwin.corn.net.dns
import pytest

SCUTIL_DATA = """
DNS configuration

resolver #1
  nameserver[0] : 192.168.1.1
  if_index : 4 (en0)
  flags    : Request A records
  reach    : 0x00020002 (Reachable,Directly Reachable Address)

resolver #2
  domain   : local
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300000

resolver #3
  domain   : 123.123.in-addr.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300200

resolver #4
  domain   : 8.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300400

resolver #5
  domain   : 9.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300600

resolver #6
  domain   : a.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300800

resolver #7
  domain   : b.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 301000

DNS configuration (for scoped queries)

resolver #1
  nameserver[0] : 192.168.1.1
  if_index : 4 (en0)
  flags    : Scoped, Request A records
  reach    : 0x00020002 (Reachable,Directly Reachable Address)"""


class TestDns:
    @pytest.mark.asyncio
    async def test_load_scutil_dns(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": SCUTIL_DATA})
        await idem_darwin.corn.net.dns.load_scutil_dns(c_hub)

        assert c_hub.corn.CORN.dns.flags == ("Request A records", "Scoped")
        assert c_hub.corn.CORN.dns.nameservers == ("192.168.1.1",)
        assert c_hub.corn.CORN.dns.options == tuple()
        assert c_hub.corn.CORN.dns.domain == tuple()
        assert c_hub.corn.CORN.dns.search == tuple()
        assert c_hub.corn.CORN.dns.sortlist == ("192.168.1.0/24",)
        assert c_hub.corn.CORN.dns.ip4_nameservers == ("192.168.1.1",)
        assert c_hub.corn.CORN.dns.ip6_nameservers == tuple()
