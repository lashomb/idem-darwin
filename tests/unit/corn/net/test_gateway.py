import idem_darwin.corn.net.gateway
import pytest

INET_DATA = """
Routing tables

Internet:
Destination        Gateway            RT_IFA             Flags        Refs      Use    Mtu   Netif Expire
default            192.168.1.1        192.168.1.24       UGSc           92        0   1500     en0
127                127.0.0.1          127.0.0.1          UCS             0        0  16384     lo0
127.0.0.1          127.0.0.1          127.0.0.1          UH              1     2049  16384     lo0
169.254            link#4             192.168.1.24       UCS             0        0   1500     en0      !
192.168.1          link#4             192.168.1.24       UCS             3        0   1500     en0      !
192.168.1.1/32     link#4             192.168.1.24       UCS             1        0   1500     en0      !
192.168.1.1        9c:3d:cf:ff:ff:ff  192.168.1.24       UHLWIir        39     1385   1500     en0     83
192.168.1.3        a4:77:33:ff:ff:ff  192.168.1.24       UHLWIi          1     9442   1500     en0   1130
192.168.1.23       3c:8d:20:ff:ff:ff  192.168.1.24       UHLWIi          2    87351   1500     en0   1130
192.168.1.24/32    link#4             192.168.1.24       UCS             0        0   1500     en0      !
192.168.1.26       f8:94:c2:f:ff:ff   192.168.1.24       UHLWI           0        0   1500     en0    439
224.0.0/4          link#4             192.168.1.24       UmCS            2        0   1500     en0      !
224.0.0.251        1:0:5e:f:f:ff      192.168.1.24       UHmLWI          0        0   1500     en0
239.255.255.250    1:0:5e:7f:ff:ff    192.168.1.24       UHmLWI          0      784   1500     en0
255.255.255.255/32 link#4             192.168.1.24       UCS             0        0   1500     en0      !
"""

INET6_DATA = """
Routing tables

Internet6:
Destination                             Gateway                         RT_IFA                                  Flags        Refs      Use    Mtu    Netif Expire
default                                 fe80::%utun0                    fe80::34f5:2eda:ffff:ffff%utun0         UGcI            0        0   1380    utun0
default                                 fe80::%utun1                    fe80::7eaf:ddab:ffff:fff%utun1          UGcI            0        0   2000    utun1
::1                                     ::1                             ::1                                     UHL             0        0  16384      lo0
fe80::%lo0/64                           fe80::1%lo0                     fe80::1%lo0                             UcI             1        0  16384      lo0
fe80::1%lo0                             link#1                          fe80::1%lo0                             UHLI            1        0  16384      lo0
fe80::%en0/64                           link#4                          fe80::cac:fdf8:ffff:ffff%en0            UCI             1        0   1500      en0
fe80::cac:fdf8:ffff:ffff%en0            88:e9:fe:ff:ff:ff               fe80::cac:fdf8:ffff:ffff%en0            UHLI            1        0  16384      lo0
fe80::%awdl0/64                         link#9                          fe80::c859:e0ff:ffff:ffff%awdl0         UCI             1        0   1484    awdl0
fe80::c859:e0ff:ffff:ffff%awdl0         ca:59:e0:ff:ff:ff               fe80::c859:e0ff:ffff:cfff%awdl0         UHLI            0        0  16384      lo0
fe80::%llw0/64                          link#10                         fe80::c859:e0ff:fff1:ffff%llw0          UCI             1        0   1500     llw0
fe80::c859:e0ff:ffff:ffff%llw0          ca:59:e0:ff:ff:ff               fe80::c859:e0ff:fff1:ffff%llw0          UHLI            0        0  16384      lo0
fe80::%utun0/64                         fe80::34f5:2eda:ffff:ffff%utun0 fe80::34f5:2eda:ffff:ffff%utun0         UcI             2        0   1380    utun0
fe80::34f5:2eda:ffff:ffff%utun0         link#11                         fe80::34f5:2eda:ffff:ffff%utun0         UHLI            0        0  16384      lo0
fe80::%utun1/64                         fe80::7eaf:ddab:ffff:fff%utun1  fe80::7eaf:ddab:ffff:fff%utun1          UcI             2        0   2000    utun1
fe80::7eaf:ddab:ffff:fff%utun1          link#12                         fe80::7eaf:ddab:ffff:fff%utun1          UHLI            0        0  16384      lo0
ff01::%lo0/32                           ::1                             ::1                                     UmCI            0        0  16384      lo0
ff01::%en0/32                           link#4                          fe80::cac:fdf8:ffff:ffff%en0            UmCI            0        0   1500      en0
ff01::%awdl0/32                         link#9                          fe80::c859:e0ff:ffff:ffff%awdl0         UmCI            0        0   1484    awdl0
ff01::%llw0/32                          link#10                         fe80::c859:e0ff:ffff:ffff%llw0          UmCI            0        0   1500     llw0
ff01::%utun0/32                         fe80::34f5:2eda:ffff:ffff%utun0 fe80::34f5:2eda:ffff:ffff%utun0         UmCI            0        0   1380    utun0
ff01::%utun1/32                         fe80::7eaf:ddab:ffff:fff%utun1  fe80::7eaf:ddab:ffff:fff%utun1          UmCI            0        0   2000    utun1
ff02::%lo0/32                           ::1                             ::1                                     UmCI            0        0  16384      lo0
ff02::%en0/32                           link#4                          fe80::cac:fdf8:ffff:ffff%en0            UmCI            0        0   1500      en0
ff02::%awdl0/32                         link#9                          fe80::c859:e0ff:ffff:ffff%awdl0         UmCI            0        0   1484    awdl0
ff02::%llw0/32                          link#10                         fe80::c859:e0ff:ffff:ffff%llw0          UmCI            0        0   1500     llw0
ff02::%utun0/32                         fe80::34f5:2eda:ffff:ffff%utun0 fe80::34f5:2eda:ffff:ffff%utun0         UmCI            0        0   1380    utun0
ff02::%utun1/32                         fe80::7eaf:ddab:ffff:fff%utun1  fe80::7eaf:ddab:ffff:fff%utun1          UmCI            0        0   2000    utun1
"""


class TestGateway:
    @pytest.mark.asyncio
    async def test_load_default_gateway(self, c_hub):

        c_hub.exec.cmd.run.side_effect = [
            c_hub.pop.data.imap({"stdout": INET_DATA}),
            c_hub.pop.data.imap({"stdout": INET6_DATA}),
        ]
        await idem_darwin.corn.net.gateway.load_default_gateway(c_hub)

        assert c_hub.corn.CORN.ip4_gw == ("192.168.1.1",)
        assert c_hub.corn.CORN.ip6_gw == ("fe80::%utun0", "fe80::%utun1")
        assert c_hub.corn.CORN.ip_gw is True
