import idem_darwin.corn.hw.cpu
import pytest


class TestCpu:
    @pytest.mark.asyncio
    async def test_load_cpu_arch(self, c_hub):

        ret = "x86_64"
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": ret})
        await idem_darwin.corn.hw.cpu.load_cpu_arch(c_hub)
        assert c_hub.corn.CORN.cpuarch == ret

    @pytest.mark.asyncio
    async def test_load_cpu_flags(self, c_hub):

        ret = (
            "FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE MCA CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE "
            "SSE2 SS HTT TM PBE SSE3 PCLMULQDQ DTES64 MON DSCPL VMX SMX EST TM2 SSSE3 FMA CX16 TPR PDCM SSE4.1 "
            "SSE4.2 x2APIC MOVBE POPCNT AES PCID XSAVE OSXSAVE SEGLIM64 TSCTMR AVX1.0 RDRAND F16C "
        )
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": ret})
        await idem_darwin.corn.hw.cpu.load_cpu_flags(c_hub)
        assert not set(c_hub.corn.CORN.cpu_flags) - set(ret.strip().lower().split(" "))

    @pytest.mark.asyncio
    async def test_load_cpu_model(self, c_hub):

        ret = "Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz"
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": ret})
        await idem_darwin.corn.hw.cpu.load_cpu_model(c_hub)
        assert c_hub.corn.CORN.cpu_model == ret

    @pytest.mark.asyncio
    async def test_load_num_cpus(self, c_hub):

        ret = 4
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": str(ret)})
        await idem_darwin.corn.hw.cpu.load_num_cpus(c_hub)
        assert c_hub.corn.CORN.num_cpus == ret
