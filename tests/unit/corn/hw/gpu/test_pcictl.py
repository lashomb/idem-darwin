import idem_darwin.corn.hw.gpu.pcictl
import pytest

PCIUTIL_DATA = """
Graphics/Displays:

    Intel Iris Plus Graphics 640:

      Chipset Model: Intel Iris Plus Graphics 640
      Type: GPU
      Bus: Built-In
      VRAM (Dynamic, Max): 1536 MB
      Vendor: Intel
      Device ID: 0x5926
      Revision ID: 0x0006
      Metal: Supported, feature set macOS GPUFamily2 v1
      Displays:
        Color LCD:
          Display Type: Built-In Retina LCD
          Resolution: 2560 x 1600 Retina
          Framebuffer Depth: 24-Bit Color (ARGB8888)
          Main Display: Yes
          Mirror: Off
          Online: Yes
          Automatically Adjust Brightness: No
          Connection Type: Internal
"""


class TestPcictl:
    @pytest.mark.asyncio
    async def test_load_data(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": PCIUTIL_DATA})
        await idem_darwin.corn.hw.gpu.pcictl.load_data(c_hub)
        assert c_hub.corn.CORN.gpus == (
            {"model": "Iris Plus Graphics 640", "vendor": "intel"},
        )
        assert c_hub.corn.CORN.num_gpus == 1
