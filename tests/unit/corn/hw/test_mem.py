import idem_darwin.corn.hw.mem
import pytest


class TestMem:
    @pytest.mark.asyncio
    async def test_load_mem(self, c_hub):

        ret = "8589934592"
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": ret})
        await idem_darwin.corn.hw.mem.load_mem(c_hub)
        assert c_hub.corn.CORN.mem_total == int(ret) // 1024 // 1024

    @pytest.mark.asyncio
    async def test_load_swap(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap(
            {"stdout": "total = 1024.00M  used = 541.75M  free = 482.25M  (encrypted)"}
        )
        await idem_darwin.corn.hw.mem.load_swap(c_hub)
        assert c_hub.corn.CORN.swap_total == 1024
