import idem_darwin.corn.hw.sysctl
import pytest


class TestSysctl:
    @pytest.mark.asyncio
    async def test_load_hwdata(self, c_hub):

        ret = "MacBookPro14,1"
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": ret})
        await idem_darwin.corn.hw.sysctl.load_hwdata(c_hub)

        assert c_hub.corn.CORN.productname == "MacBookPro14,1"
