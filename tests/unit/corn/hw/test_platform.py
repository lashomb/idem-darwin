import idem_darwin.corn.hw.platform
import pytest

PLATFORM_DATA = """
Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro14,1
      Processor Name: Dual-Core Intel Core i5
      Processor Speed: 2.3 GHz
      Number of Processors: 1
      Total Number of Cores: 2
      L2 Cache (per Core): 256 KB
      L3 Cache: 4 MB
      Hyper-Threading Technology: Enabled
      Memory: 8 GB
      Boot ROM Version: 205.0.0.0.0
      SMC Version (system): 2.43f9
      Serial Number (system): ABCD12EFGH34
"""


class TestPlatform:
    @pytest.mark.asyncio
    async def test_load_profiler(self, c_hub):
        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": PLATFORM_DATA})

        await idem_darwin.corn.hw.platform.load_profiler(c_hub)

        assert c_hub.corn.CORN.model_name == "MacBook Pro"
        assert c_hub.corn.CORN.boot_rom_version == "205.0.0.0.0"
        assert c_hub.corn.CORN.smc_version == "2.43f9"
        assert c_hub.corn.CORN.serialnumber == "ABCD12EFGH34"
