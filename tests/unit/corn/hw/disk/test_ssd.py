import idem_darwin.corn.hw.disk.ssd
import pytest

DISKUTIL_LIST = """
<plist version="1.0">
<dict>
	<key>AllDisks</key>
	<array>
		<string>disk0</string>
		<string>disk0s1</string>
		<string>disk0s2</string>
		<string>disk1</string>
		<string>disk1s1</string>
		<string>disk1s2</string>
		<string>disk1s3</string>
		<string>disk1s4</string>
		<string>disk1s5</string>
	</array>
	<key>WholeDisks</key>
	<array>
		<string>disk0</string>
		<string>disk1</string>
	</array>
</dict>
</plist>
"""

DISKUTIL_HHD = """
<plist version="1.0">
<dict>
	<key>SolidState</key>
	<false/>
</dict>
</plist>
"""

DISKUTIL_SSD = """
<plist version="1.0">
<dict>
	<key>SolidState</key>
	<true/>
</dict>
</plist>
"""


class TestSsd:
    @pytest.mark.asyncio
    async def test_load_disks(self, c_hub):

        c_hub.exec.cmd.run.side_effect = [
            c_hub.pop.data.imap({"stdout": DISKUTIL_LIST}),
            c_hub.pop.data.imap({"stdout": DISKUTIL_HHD}),
            c_hub.pop.data.imap({"stdout": DISKUTIL_SSD}),
        ]
        await idem_darwin.corn.hw.disk.ssd.load_disks(c_hub)
        assert c_hub.corn.CORN.disks == ("disk0", "disk1")
        assert c_hub.corn.CORN.SSDs == ("disk1",)
