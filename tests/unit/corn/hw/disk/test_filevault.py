import idem_darwin.corn.hw.disk.filevault
import pytest


class TestFilevault:
    @pytest.mark.asyncio
    async def test_load_filevault_enabled(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap(
            {"stdout": "FileVault is On."}
        )
        await idem_darwin.corn.hw.disk.filevault.load_filevault_enabled(c_hub)
        assert c_hub.corn.CORN.filevault is True

    @pytest.mark.asyncio
    async def test_load_filevault_disabled(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap(
            {"stdout": "FileVault is Off."}
        )
        await idem_darwin.corn.hw.disk.filevault.load_filevault_enabled(c_hub)
        assert c_hub.corn.CORN.filevault is False
