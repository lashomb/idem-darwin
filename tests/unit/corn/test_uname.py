import idem_darwin.corn.uname
import os
import pytest
import unittest.mock as mock


class TestUname:
    @pytest.mark.asyncio
    async def test_load_uname(self, c_hub):

        with mock.patch.object(
            os,
            "uname",
            return_value=("Darwin", "test_node", "test_release", "test_version", ""),
        ):
            await idem_darwin.corn.uname.load_uname(c_hub)

        assert c_hub.corn.CORN.kernel == "Darwin"
        assert c_hub.corn.CORN.nodename == "test_node"
        assert c_hub.corn.CORN.kernelrelease == "test_release"
        assert c_hub.corn.CORN.kernelversion == "test_version"

        # Hard coded grains, these should never change
        assert c_hub.corn.CORN.init == "launchd"
        assert c_hub.corn.CORN.osmanufacturer == "Apple Inc."
        assert c_hub.corn.CORN.manufacturer == "Apple Inc."
        assert c_hub.corn.CORN.os_family == "MacOS"
        assert c_hub.corn.CORN.ps == "ps auxwww"
