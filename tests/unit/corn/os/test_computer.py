import idem_darwin.corn.os.computer
import pytest


class TestComputer:
    @pytest.mark.asyncio
    async def test_load_computer_name(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": "test"})

        await idem_darwin.corn.os.computer.load_computer_name(c_hub)

        assert c_hub.corn.CORN.computer_name == "test"
