import idem_darwin.corn.os.arch
import os
import pytest
import unittest.mock as mock


class TestArch:
    @pytest.mark.asyncio
    async def test_load_osarch(self, c_hub):

        ret = lambda: 0
        ret.machine = "x86_64"
        with mock.patch.object(os, "uname", return_value=ret):
            await idem_darwin.corn.os.arch.load_osarch(c_hub)

        assert c_hub.corn.CORN.osarch == ret.machine
