import idem_darwin.corn.os.sw_vers
import pytest


class TestSwVers:
    @pytest.mark.asyncio
    async def test_load_os_build(self, c_hub):

        c_hub.exec.cmd.run.return_value = c_hub.pop.data.imap({"stdout": "19E266"})

        await idem_darwin.corn.os.sw_vers.load_os_build(c_hub)

        assert c_hub.corn.CORN.osbuild == "19E266"

    @pytest.mark.asyncio
    async def test_load_os_release(self, c_hub):

        c_hub.exec.cmd.run.side_effect = [
            c_hub.pop.data.imap({"stdout": "10.15.4"}),
            c_hub.pop.data.imap({"stdout": "Mac OS X"}),
        ]

        await idem_darwin.corn.os.sw_vers.load_os_release(c_hub)

        assert c_hub.corn.CORN.osrelease == "10.15.4"
        assert c_hub.corn.CORN.osfullname == "Mac OS X 10.15.4"
        assert c_hub.corn.CORN.osrelease_info == (10, 15, 4)
        assert c_hub.corn.CORN.osmajorrelease == 10
        assert c_hub.corn.CORN.os == "macOS"
        assert c_hub.corn.CORN.oscodename == "Catalina"
        assert c_hub.corn.CORN.osfinger == "Mac OS X 10.15.4-10"
