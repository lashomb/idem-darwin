import idem_darwin.corn.proc.user
import os
import pytest
import pwd
import unittest.mock as mock


class TestUser:
    @pytest.mark.asyncio
    async def test_load_user(self, c_hub):

        with mock.patch.object(os, "geteuid", return_value=99):
            ret = lambda: 0
            ret.pw_name = "test"
            with mock.patch.object(pwd, "getpwuid", return_value=ret):
                await idem_darwin.corn.proc.user.load_user(c_hub)

        assert c_hub.corn.CORN.uid == 99
        assert c_hub.corn.CORN.username == "test"
