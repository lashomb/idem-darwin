import idem_darwin.corn.proc.group
import grp
import os
import pytest
import unittest.mock as mock


class TestGroup:
    @pytest.mark.asyncio
    async def test_load_group(self, c_hub):

        with mock.patch.object(os, "getegid", return_value=99):
            ret = lambda: 0
            ret.gr_name = "test"
            with mock.patch.object(grp, "getgrgid", return_value=ret):
                await idem_darwin.corn.proc.group.load_group(c_hub)

        assert c_hub.corn.CORN.gid == 99
        assert c_hub.corn.CORN.groupname == "test"
