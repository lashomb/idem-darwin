import idem_darwin.corn.proc.pid
import os
import pytest
import unittest.mock as mock


class TestPid:
    @pytest.mark.asyncio
    async def test_load_pid(self, c_hub):

        ret = 8972358735
        with mock.patch.object(os, "getpid", return_value=ret):
            await idem_darwin.corn.proc.pid.load_pid(c_hub)

        assert c_hub.corn.CORN.pid == ret
