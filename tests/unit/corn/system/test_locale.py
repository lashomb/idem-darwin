import idem_darwin.corn.system.locale
import locale
import pytest
import sys
import time
import unittest.mock as mock


class TestLocale:
    @pytest.mark.asyncio
    async def test_load_info(self, c_hub):

        with mock.patch.object(
            locale, "getdefaultlocale", return_value=("test_lang", "test_enc")
        ):
            await idem_darwin.corn.system.locale.load_info(c_hub)

        assert c_hub.corn.CORN.locale_info.defaultencoding == "test_enc"
        assert c_hub.corn.CORN.locale_info.defaultlanguage == "test_lang"

    @pytest.mark.asyncio
    async def test_load_info(self, c_hub):

        with mock.patch.object(sys, "getdefaultencoding", return_value="test"):
            await idem_darwin.corn.system.locale.load_default_encoding(c_hub)

        assert c_hub.corn.CORN.locale_info.detectedencoding == "test"

    @pytest.mark.asyncio
    async def test_load_timezone(self, c_hub):

        val = time.tzname
        time.tzname = ("ZZZ", "ZZZ")
        await idem_darwin.corn.system.locale.load_timezone(c_hub)
        time.tzname = val

        assert c_hub.corn.CORN.locale_info.timezone == "ZZZ"
