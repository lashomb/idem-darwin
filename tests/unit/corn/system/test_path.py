import idem_darwin.corn.system.path
import os
import pytest
import sys
import unittest.mock as mock


class TestPath:
    @pytest.mark.asyncio
    async def test_load_cwd(self, c_hub):

        ret = "test"
        with mock.patch.object(os, "getcwd", return_value=ret):
            await idem_darwin.corn.system.path.load_cwd(c_hub)

        assert c_hub.corn.CORN.cwd == ret

    @pytest.mark.asyncio
    async def test_load_executable(self, c_hub):

        origin = sys.executable
        sys.executable = "test"
        await idem_darwin.corn.system.path.load_executable(c_hub)
        sys.executable = origin

        assert c_hub.corn.CORN.pythonexecutable == "test"

    @pytest.mark.asyncio
    async def test_load_path(self, c_hub):

        with mock.patch.object(os.environ, "get", return_value="test:test1:test2"):
            await idem_darwin.corn.system.path.load_path(c_hub)

        assert c_hub.corn.CORN.path == "test:test1:test2"

    @pytest.mark.asyncio
    async def test_load_pythonpath(self, c_hub):

        origin = sys.path
        sys.path = ["test", "test1", "test2"]
        await idem_darwin.corn.system.path.load_pythonpath(c_hub)
        sys.path = origin

        assert c_hub.corn.CORN.pythonpath == ("test", "test1", "test2")
