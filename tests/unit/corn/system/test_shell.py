import idem_darwin.corn.system.shell
import os
import pytest
import unittest.mock as mock


class TestShell:
    @pytest.mark.asyncio
    async def test_load_shell(self, c_hub):

        ret = "/bin/test_shell"
        with mock.patch.object(os.environ, "get", return_value=ret):
            await idem_darwin.corn.system.shell.load_shell(c_hub)

        assert c_hub.corn.CORN.shell == ret
