import idem_darwin.corn.system.console
import os
import pytest
import pwd
import unittest.mock as mock


class TestConsole:
    @pytest.mark.asyncio
    async def test_load_console_user(self, c_hub):

        ret = lambda: 0
        ret.pw_name = "test"
        with mock.patch.object(os.path, "exists", return_value=True):
            with mock.patch.object(os, "stat", return_value=(0, 1, 2, 3, 99)):
                with mock.patch.object(pwd, "getpwuid", return_value=ret):
                    await idem_darwin.corn.system.console.load_console_user(c_hub)

        assert c_hub.corn.CORN.console_user == 99
        assert c_hub.corn.CORN.console_username == "test"

    @pytest.mark.asyncio
    async def test_load_console_user_root(self, c_hub):

        with mock.patch.object(os.path, "exists", return_value=False):
            await idem_darwin.corn.system.console.load_console_user(c_hub)

        assert c_hub.corn.CORN.console_user == 0
        assert c_hub.corn.CORN.console_username == "root"
